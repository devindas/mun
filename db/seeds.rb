# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

committees = %w(ga1 ga3 ga6 ecosoc slead hosst unesco unfccc unhrc sc wb)
countries = %w(Brazil China France Germany India Nigeria Russia SaudiArabia UK America Iran SouthAfrica Egypt Serbia Venezuela)
6.times do |i|
  Day.create(day: i + 1)
  committees.each do |committee|
    x = Committee.create(day_id: i + 1, name: committee)
    countries.each do |country|
      d = Delegate.create(committee_id: x.id, country: country, point: 0, session1: false, session2: false, poi_score: 0, poi_count: 1)
      # # t = ModTopic.create(delegate_id: d.id,title: "Mod Topic by #{d.country}", mark: rand(1..5))
      # countries.each do |_country|
      #   # ModSpeech.create(mod_topic_id: t.id, country: _country, mark: rand(1..5)) if d.country != _country
      # end
    end
  end
end
