class AddIndexToDelegates < ActiveRecord::Migration
  def change
    add_index :delegates, :country
  end
end
