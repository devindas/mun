class AddPoiToDelegates < ActiveRecord::Migration
  def change
    add_column :delegates, :poi_score, :integer
    add_column :delegates, :poi_count, :integer
  end
end
