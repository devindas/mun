class CreateModTopics < ActiveRecord::Migration
  def change
    create_table :mod_topics do |t|
      t.belongs_to :delegate
      t.string :title
      t.integer :mark
      t.timestamps null: false
    end
    add_index :mod_topics, [:delegate_id, :title], unique: true
    add_index :mod_topics, :delegate_id
  end
end
