class AddSpeechToDelegates < ActiveRecord::Migration
  def change
    add_column :delegates, :speech, :integer, default: nil
  end
end
