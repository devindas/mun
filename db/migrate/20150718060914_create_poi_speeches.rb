class CreatePoiSpeeches < ActiveRecord::Migration
  def change
    create_table :poi_speeches do |t|
      t.belongs_to :delegate
      t.integer :mark
      t.string :country
      t.timestamps null: false
    end
    add_index :poi_speeches, [:country, :delegate_id], unique: true
  end
end
