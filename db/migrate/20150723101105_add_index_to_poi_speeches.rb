class AddIndexToPoiSpeeches < ActiveRecord::Migration
  def change
    add_index :poi_speeches, :country
  end
end
