class CreateCommittees < ActiveRecord::Migration
  def change
    create_table :committees do |t|
      t.belongs_to :day, index: true
      t.string :name

      t.timestamps null: false
    end
    add_index :committees, [:name, :day_id], unique: true
  end
end
