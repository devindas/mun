class CreateModSpeeches < ActiveRecord::Migration
  def change
    create_table :mod_speeches do |t|
      t.belongs_to :mod_topic
      t.string :country
      t.integer :mark
      t.timestamps null: false
    end
    add_index :mod_speeches, [:mod_topic_id, :country], unique: true
    add_index :mod_speeches, :country
  end
end
