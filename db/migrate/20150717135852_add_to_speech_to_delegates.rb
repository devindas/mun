class AddToSpeechToDelegates < ActiveRecord::Migration
  def change
    add_column :delegates, :to_speech, :boolean, default: false
  end
end
