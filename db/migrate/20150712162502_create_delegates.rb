class CreateDelegates < ActiveRecord::Migration
  def change
    create_table :delegates do |t|
      t.belongs_to :committee
      t.string :country
      t.integer :point
      t.boolean :session1
      t.boolean :session2

      t.timestamps null: false
    end
    add_index :delegates, [:country, :committee_id], unique: true
  end
end
