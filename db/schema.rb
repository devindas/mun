# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150725230650) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "committees", force: :cascade do |t|
    t.integer  "day_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "committees", ["day_id"], name: "index_committees_on_day_id", using: :btree
  add_index "committees", ["name", "day_id"], name: "index_committees_on_name_and_day_id", unique: true, using: :btree

  create_table "days", force: :cascade do |t|
    t.integer  "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "days", ["day"], name: "index_days_on_day", unique: true, using: :btree

  create_table "delegates", force: :cascade do |t|
    t.integer  "committee_id"
    t.string   "country"
    t.integer  "point"
    t.boolean  "session1"
    t.boolean  "session2"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "speech"
    t.boolean  "to_speech",    default: false
    t.integer  "poi_score"
    t.integer  "poi_count"
  end

  add_index "delegates", ["country", "committee_id"], name: "index_delegates_on_country_and_committee_id", unique: true, using: :btree
  add_index "delegates", ["country"], name: "index_delegates_on_country", using: :btree

  create_table "mod_speeches", force: :cascade do |t|
    t.integer  "mod_topic_id"
    t.string   "country"
    t.integer  "mark"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "mod_speeches", ["country"], name: "index_mod_speeches_on_country", using: :btree
  add_index "mod_speeches", ["mod_topic_id", "country"], name: "index_mod_speeches_on_mod_topic_id_and_country", unique: true, using: :btree

  create_table "mod_topics", force: :cascade do |t|
    t.integer  "delegate_id"
    t.string   "title"
    t.integer  "mark"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "mod_topics", ["delegate_id", "title"], name: "index_mod_topics_on_delegate_id_and_title", unique: true, using: :btree
  add_index "mod_topics", ["delegate_id"], name: "index_mod_topics_on_delegate_id", using: :btree

  create_table "poi_speeches", force: :cascade do |t|
    t.integer  "delegate_id"
    t.integer  "mark"
    t.string   "country"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "poi_speeches", ["country", "delegate_id"], name: "index_poi_speeches_on_country_and_delegate_id", unique: true, using: :btree
  add_index "poi_speeches", ["country"], name: "index_poi_speeches_on_country", using: :btree

end
