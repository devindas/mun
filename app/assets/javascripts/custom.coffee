$(document).on 'ready page:load', ->
  $('.collapsible').collapsible accordion: false
  $('select').material_select()
  $('.modal-trigger').leanModal()
  return
