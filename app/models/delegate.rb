class Delegate < ActiveRecord::Base
  belongs_to :committee
  has_many :poi_speeches, dependent: :destroy
  has_many :mod_topics, dependent: :destroy
  default_scope -> { order(country: :asc) }
  validates :speech, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 10 }, allow_blank: true

  def poi_speeches
    PoiSpeech.where('delegate_id = ?', id)
  end
  def has_poi?
    PoiSpeech.where('delegate_id = ?', id).any?
  end
  def mod_topics
    ModTopic.where('delegate_id = ?', id)
  end
end
