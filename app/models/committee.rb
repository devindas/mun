class Committee < ActiveRecord::Base
  belongs_to :day
  has_many :delegates, dependent: :destroy

  def delegates
    Delegate.where('committee_id = ?', id)
  end

  def absent_delegates(session_num)
    Delegate.where("committee_id = ? AND session#{session_num} = false", id)
  end

  def spoken_delegates
    Delegate.where('speech IS NOT NULL AND committee_id = ?', id)
  end

  def to_speak_delegates
    Delegate.where('to_speech IS TRUE AND speech IS NULL AND committee_id = ?', id)
  end

  def not_to_speak_delegates
    Delegate.where('to_speech IS FALSE AND committee_id = ?', id)
  end

  def filter_delegates(string)
    Delegate.where("LOWER(country) like lower('%#{string}%') AND committee_id = ? AND to_speech IS TRUE", id)
  end

  def poi_delegates(country)
    delegates = []
    delegate_to = Delegate.find_by(country: country, committee_id: id)
    valid_poi_delegates = Delegate.where("country != '#{country}' AND committee_id = ?", id)
    valid_poi_delegates.each do |delegate|
      delegates.push(delegate) unless PoiSpeech.find_by(country: delegate.country, delegate_id: delegate_to.id)
    end
    delegates
  end

  def mod_topics
    ModTopic.where('delegate_id IN (?)', delegates.ids)
  end
end
