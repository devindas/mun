class ModTopic < ActiveRecord::Base
  belongs_to :delegate
  has_many :mod_speeches, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  validates :mark, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 10 }, allow_blank: true

  def mod_speeches
    ModSpeech.where('mod_topic_id = ?', id)
  end

  def has_mod_speeches?
    ModSpeech.where('mod_topic_id = ?', id).any?
  end


end
