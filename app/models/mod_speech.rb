class ModSpeech < ActiveRecord::Base
  belongs_to :mod_topic
  default_scope -> { order(created_at: :desc) }
  validates :mark, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 10 }, allow_blank: true

end
