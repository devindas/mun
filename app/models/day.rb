class Day < ActiveRecord::Base
  has_many :committees, dependent: :destroy

  def committees
    Committee.where('day_id = ?', id)
  end
end
