class RolecallController < ApplicationController
  def index
    @session_id = 1
    @delegates = Day.first.committees.first.absent_delegates(1).all
  end

  def complete
    @session_id = params[:session_id]
    @delegate_ids = params[:delegate_ids]
    puts @session_id
    if @session_id == '1'
      Delegate.where('id IN (?)', @delegate_ids).update_all(session1: true)
    elsif @session_id == '2'
      Delegate.where('id IN (?)', @delegate_ids).update_all(session2: true)
    end
    @delegates = Day.first.committees.first.absent_delegates(@session_id).all
    respond_to do |format|
      format.html { redirect_to rolecall_index_path}
      format.js
    end
  end

  def update_session_id
    @session_id = params[:session_id]
    @delegates = Day.first.committees.first.absent_delegates(@session_id).all
  end
end
