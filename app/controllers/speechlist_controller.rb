class SpeechlistController < ApplicationController
  include SpeechlistHelper
  def index
  end

  def complete
    @delegate_ids = params[:delegate_ids]
    Delegate.where('id IN (?)', @delegate_ids).update_all(to_speech: true)
    @delegates = Day.first.committees.first.absent_delegates(@session_id).all
    respond_to do |format|
      format.html { redirect_to speechlist_index_path }
      format.js
    end
  end

  def update_mark
    @delegate_id = params[:delegate_id]
    @mark = params[:mark]
    Delegate.find(@delegate_id).update(speech: @mark)
    respond_to do |format|
      format.html { redirect_to speechlist_index_path }
      format.js
    end
  end

  def filter
    @name = params[:country_name]
    @delegates = filter_delegates(@name)
  end

  def update_poi
    @delegate_to = Day.first.committees.first.delegates.find(params[:delegate_id])
    @delegate_from = Day.first.committees.first.delegates.find_by_country(params[:country])
    @poi_speech = @delegate_to.poi_speeches.find_by_country(params[:country])
    @new_mark = params[:mark].to_i
    @delegate_from.update(poi_score: @delegate_from.poi_score + (@new_mark - @poi_speech.mark))
    @poi_speech.update(mark: @new_mark)
    respond_to do |format|
      format.html { redirect_to speechlist_index_path }
      format.js
    end
  end

  def add_poi
    @delegate_to = params[:delegate_to]
    @delegate_to_country = params[:delegate_to_country]
    @delegate_ids = Delegate.find(params[:delegate_ids])
    @delegate_ids.each do |delegate|
      PoiSpeech.create(delegate_id: @delegate_to, country: delegate.country)
    end
  end
  def delete_mark
    @delegate_id = params[:delegate_id]
    if Delegate.find(@delegate_id)
      Delegate.where('id = ?', @delegate_id).update_all(speech: nil, to_speech: false)
      @delegate_country = Delegate.find(params[:delegate_id]).country
    end
  end
  def delete_poi
    @delegate_to = params[:delegate_to].to_i
    @delegate_from = params[:delegate_by]
    if PoiSpeech.find_by(delegate_id: @delegate_to, country: @delegate_from).destroy
      @delegate_to = Delegate.find(@delegate_to)
    end
  end
end
