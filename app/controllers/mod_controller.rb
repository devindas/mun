class ModController < ApplicationController
  def add_topic
    @delegate_id = params[:delegate_id]
    @delegate_country = Delegate.find(@delegate_id).country
    @title = params[:title]
    ModTopic.create(delegate_id: @delegate_id, title: @title)
  end

  def delete_topic
    @topic_id = params[:topic_id]
    @topic = ModTopic.find(@topic_id)
    @topic.destroy
  end

  def update_topic_mark
    @mark = params[:mark]
    @topic_id = params[:topic_id]
    ModTopic.where('id = ?', @topic_id).update_all(mark: @mark.to_i)
    @topic = ModTopic.find(@topic_id)
  end

  def update_speech
    @topic_id = params[:topic_id]
    @mark = params[:mark]
    @delegate_by_country = params[:country]
    ModSpeech.where("mod_topic_id = #{@topic_id} AND country = '#{@delegate_by_country}'").update_all(mark: @mark.to_i)
  end

  def delete_speech
    @speech_id = params[:speech_id]
    @speech_country = ModSpeech.find(@speech_id).country
    ModSpeech.find(@speech_id).destroy
  end

  def add_speech
    @topic_id = params[:topic_id]
    @delegate_ids = params[:delegate_ids]
    @delegate_to_country = params[:delegate_to_country]

    @delegate_ids = Delegate.find(params[:delegate_ids])
    @delegate_ids.each do |delegate|
      ModSpeech.create(mod_topic_id: @topic_id, country: delegate.country)
    end
  end
end
