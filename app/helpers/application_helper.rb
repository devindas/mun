module ApplicationHelper
  def active_navigation(item, action)
    'active' if action == item
  end
end
