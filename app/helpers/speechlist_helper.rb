module SpeechlistHelper
  def spoken_delegates
    Day.first.committees.first.spoken_delegates
  end

  def to_speak_delegates
    Day.first.committees.first.to_speak_delegates
  end

  def not_to_speak_delegates
    Day.first.committees.first.not_to_speak_delegates
  end

  def filter_delegates(string)
    Day.first.committees.first.filter_delegates(string)
  end

  def has_spoken(delegate)
    'blue-grey lighten-5' if delegate.speech?
  end

  def poi_delegates(country)
    Day.first.committees.first.poi_delegates(country)
  end
end
