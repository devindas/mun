module ModHelper
  def mod_topics
    Day.first.committees.first.mod_topics
  end
  def delegates
    Day.first.committees.first.delegates
  end

  def speech_delegates(topic_id)
    delegates =[]
    @delegate_to = Delegate.find(ModTopic.find(topic_id).delegate_id)
    valid_topic_delegates = Delegate.where("country != '#{@delegate_to.country}' AND committee_id = ?", @delegate_to.committee_id)
    valid_topic_delegates.each do |delegate|
      delegates.push(delegate) unless ModSpeech.find_by(country: delegate.country, mod_topic_id: topic_id )
    end
    delegates
    end
end
