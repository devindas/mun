Rails.application.routes.draw do
  resources :rolecall, only: [:index] do
    collection do
      put :complete
      get :update_session_id
    end
  end
  resources :speechlist, only: [:index] do
    collection do
      put :complete
      post :add_poi
      get :update_poi
      get :update_mark
      get :filter
      get :delete_poi
      get :delete_mark
    end
  end
  resources :mod, only: [:index] do
    collection do
      post :add_topic
      get :delete_topic
      get :update_topic_mark

      get :update_speech
      get :delete_speech
      post :add_speech
    end
  end
  root to: 'pages#index'
end
